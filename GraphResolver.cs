﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GraphResolverTuples
{
    public class GraphResolver
    {
        private static List<List<int>> _paths;
        private static Dictionary<int, LinkedList<int>> _destinations;

        public static List<List<int>> ConnectingPaths(List<Tuple<int, int>> graph, int start, int end)
        {
            _paths = new List<List<int>>();
            _destinations = new Dictionary<int, LinkedList<int>>();

            foreach (var destination in graph)
            {
                if (!_destinations.ContainsKey(destination.Item1))
                {
                    _destinations.Add(destination.Item1, new LinkedList<int>());
                }

                _destinations[destination.Item1].AddLast(destination.Item2);
            }

            BuildPaths(start, end, new List<int>());
            return _paths;
        }

        private static void BuildPaths(int start, int end, List<int> path)
        {
            if (!_destinations.ContainsKey(start)) return;

            _destinations.TryGetValue(start, out var destinations);

            if (destinations == null) return;

            foreach (var destination in destinations)
            {
                if (path.Count < 1) path.Add(start);

                path.Add(destination);

                if (destination != end)
                {
                    BuildPaths(destination, end, path);
                }
                else if (destination == end)
                {
                    _paths.Add(path.ToList());
                    path.Clear();
                }
            }
        }
    }
}
