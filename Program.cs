﻿using System;
using System.Linq;

namespace GraphResolverTuples
{
    class Program
    {
        static void Main(string[] args)
        {
            GraphResolver.ConnectingPaths(new[]{

                //Tuple.Create(1, 2),
                //Tuple.Create(1, 3),
                //Tuple.Create(2, 4),
                //Tuple.Create(3, 4),
                //Tuple.Create(4, 5),
                //Tuple.Create(4, 6),
                
                Tuple.Create(9, 2),
                Tuple.Create(9, 3),
                Tuple.Create(2, 4),
                Tuple.Create(3, 4),
                Tuple.Create(14, 15),
                Tuple.Create(3, 14),
                Tuple.Create(4, 14),

            }.ToList(), 9, 15);
        }
    }
}